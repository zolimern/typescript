var first: number = 5
var first = 10

let num: number = 12.12

let text: string = 'some'

let is : boolean = false

let a : null = null

let b : undefined = undefined

let c: any = 23234

c = 'some'

let d : unknown = 'alma'
d = true
console.log(d)

let vAny: any = 10;          // We can assign anything to any
let vUnknown: unknown =  "20"; // We can assign anything to unknown just like any 


let s1: string = vAny;     // Any is assignable to anything 
let s2: string = String(vUnknown); // Invalid; we can't assign vUnknown to any other type (without an explicit assertion)


var ismeretlen: unknown = 'alma'
ismeretlen=5
ismeretlen=true

ismeretlen = "szamok" 
document.write(String(ismeretlen))

let textArr: string[] = ["alma", "korte", "some", "any"]

let textArr2 : Array<string> = ["fps", "any", "some", "full"]

let szTomb: Array<number> = [3,2,3,234,34]


